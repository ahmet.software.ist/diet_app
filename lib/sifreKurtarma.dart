import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SifreKurtarma extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF252525),
        body: Center(
          child: Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.8,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(child: Image.asset(""),width: 150,height: 150,),
                  Text("Şifre Kurtarma",
                      style: TextStyle(color: Colors.green, fontSize: 24)),
                  Text(
                      "Lütfen DietM hesabınızla ilişkili e-posta adresini veya üye adınızı giriniz",
                      style: TextStyle(color: Colors.green, fontSize: 24),
                      textAlign: TextAlign.center),
                  TextField(
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                            BorderSide(color: Colors.green, width: 3)),
                        labelText: 'E-Posta',
                        labelStyle:
                        TextStyle(fontSize: 20, color: Colors.green)),
                  ),
                  CupertinoButton(
                    child: Text("Devam Et"),
                    onPressed: ()=>{},
                    color: Colors.green,
                  )
                ],
              )),
        ),
      ),
    );
  }
}